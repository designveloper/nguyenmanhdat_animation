* 3d transforms

        :::css
        .wrapper {
          perspective: 1000px;
        }
        img {
          display:block;
          transform: rotateY(45deg);
        }

* CSS transitions

        :::css
        transition: background .5s ease-in, transform .25s ease-in-out;

* Basic animation

        :::css
        .robot {
          animation-name: slide;
          animation-duration: 2s;
          animation-timing-function: ease-in;
          animation-iteration-count: 2;
          animation-delay: 1s;
	        animation-fill-mode: forwards;
          animation-direction: alternate;
        }
        @keyframes slide {
          from {transform: translateX(0);}
          to {transform: translateX(900px);}
        }

* Shorthand animation

        :::css
        animation: drift 35s 10s linear infinite backwards;

* Pause and play animation

        :::css
        .sticker {
          animation: spin 10s linear infinite;
          animation-play-state:paused;
        }
        .sticker:hover {
          animation-play-state:running;
        }